<?php include 'header.php'; ?>
<div class="container account">
    <div class="row">
        <div class="col-sm-8 col-md-9 col-xs-12 col-lg-10 pull-right">
            <div class="row bg-white-no-padding">
                <div class="col-xs-12">
                    <ul class="list-inline list-order-info">
                        <li>
                            <div class="media">
                            <div class="media-left">
                                    <i class="material-icons md-18">assignment</i>
                                </div>
                                <div class="media-body">
                                    <p>Nomor pesanan: <br>
                                    <span class="dark-grey">12332121312</span><span class="grey"> - Dec, 18 2016 pukul 21:37</span>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                            <div class="media-left">
                                    <i class="material-icons md-18">unarchive</i>
                                </div>
                                <div class="media-body">
                                    <p>Total barang: <br>
                                    <span class="dark-grey">10</span>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                            <div class="media-left">
                                    <i class="material-icons md-18">people</i>
                                </div>
                                <div class="media-body">
                                    <p>Jumlah penjual: <br>
                                    <span class="dark-grey">1</span>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                            <div class="media-left">
                                    <i class="material-icons md-18">receipt</i>
                                </div>
                                <div class="media-body">
                                    <p>Total pembayaran: <br>
                                    <span class="dark-grey">Rp91.000.000</span>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                            <div class="media-left">
                                    <i class="material-icons md-18">credit_card</i>
                                </div>
                                <div class="media-body">
                                    <p>Metode pembayaran: <br>
                                    <span class="dark-grey">Bank transfer</span>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                            <div class="media-left">
                                    <i class="material-icons md-18">timeline</i>
                                </div>
                                <div class="media-body">
                                    <p>Status: <br>
                                    <span class="dark-grey">Dalam proses</span>
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div> <!-- row -->
            <div class="row bg-white-no-padding no-padding">
                <div class="col-xs-12 no-padding">
                    <div class="table-responsive">
                        <table class="table table-myorder-vendor">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="pull-left">Pembelian dari penjual <a href="">Viraindo</a> - Nomor pesanan <span class="black"><b>#1239490123</b></span></div>
                                        <div class="pull-right">
                                            <p class="pull-left"><b class="black">E-faktur:</b> <span class="grey">Belum tersedia</span></p> <p class="pull-right"><a href=""><i class="material-icons md-18">rotate_right</i> Pesan ulang</a></p>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="tracking-status">
                                            <ul class="tracker-progress list-unstyled">
                                                <li class="active"><i class="material-icons md-24">schedule</i></li>
                                                <li class=""><i class="material-icons md-24">done</i></li>
                                                <li class=""><i class="material-icons md-24">local_shipping</i></li>
                                                <li class=""><i class="material-icons md-24">done_all</i></li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <p><b>Status:</b> <span class="red"><b>2</b></span> dari <span class="red"><b>3</b></span> pesanan diterima dan sedang dipersiapkan penjual</p>
                                            <p class="grey"><small>18 Agustus 2017 pukul 21:37</small></p>
                                        </div>
                                        <div class="collapse more-detail" id="collapseExample">
                                            <table>
                                                <tr>
                                                    <td width="50%">
                                                        <div class="detail pull-left">
                                                            <p>
                                                                <b>BCA KCP Mal Pondok Indah II</b><br>
                                                                Jl. Kencana Indah III No. 28
                                                                Pondok Indah, Jakarta Selatan
                                                                19200, Indonesia
                                                            </p>
                                                        </div>
                                                        <div class="detail pull-left">
                                                            <p><i class="material-icons md-18">person</i> Baydiwo <br>
                                                            <i class="material-icons md-18">call</i> Baydiwo</p>
                                                        </div>
                                                    </td>
                                                    <td width="50%">
                                                        <p><b>Metode pengiriman</b> <br>
                                                            Bizzy Fulfillment
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table class="table table-bordered table-myorder">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <a href="#">
                                                                    <img class="media-object" src="build/img/laptop-thumb.png" alt="...">
                                                                    </a>
                                                                </div>
                                                                <div class="media-body">
                                                                    <p><b>Apple Macbook Pro 256SSD - SV100021HC (Extended Warranty)</b></p>
                                                                    <span>Estimasi pengiriman <i>3-7 hari</i></span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>Status: <br> <span class="green">Diterima</span></td>
                                                        <td>Keterangan: <br> - </td>
                                                        <td><a href="#" class="btn btn-primary btn-alternative-vendor" disabled>Penjual alternatif</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <a href="#">
                                                                    <img class="media-object" src="build/img/laptop-thumb.png" alt="...">
                                                                    </a>
                                                                </div>
                                                                <div class="media-body">
                                                                    <p><b>Apple Macbook Pro 256SSD - SV100021HC (Extended Warranty)</b></p>
                                                                    <span>Estimasi pengiriman <i>3-7 hari</i></span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>Status: <br> <span class="red">Diterima</span></td>
                                                        <td>Keterangan: <br> - </td>
                                                        <td><a href="#"  id="buy"  class="btn btn-primary btn-alternative-vendor">Penjual alternatif</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="table table-no-bordered table-order-total">
                                                <tbody>
                                                    <tr>
                                                        <td>Subtotal</td>
                                                        <td>Rp99.9999</td>
                                                    </tr>
                                                    <tr>
                                                        <td>PPn 10%</td>
                                                        <td>Rp99.9999</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pengiriman</td>
                                                        <td>Rp99.9999</td>
                                                    </tr>
                                                    <tr class="discount">
                                                        <td>Discount (Bizzy Berkontribusi)</td>
                                                        <td>-Rp99.9999</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total tagihan</td>
                                                        <td>Rp999.999</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div> <!-- collapse -->
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a class="btn-more-detail" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" data-text-swap="Sembunyikan detil pesanan" data-text-original="Lihat detil pesanan">Lihat detil pesanan</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> <!-- table responsive -->
                </div> <!-- row -->
            </div> <!-- bg white -->

            <div class="row bg-white-no-padding no-padding">
                <div class="col-xs-12 no-padding">
                    <div class="table-responsive">
                        <table class="table table-myorder-vendor">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="pull-left">Pembelian dari penjual <a href="">Viraindo</a> - Nomor pesanan <span class="black"><b>#1239490123</b></span></div>
                                        <div class="pull-right">
                                            <p class="pull-left"><b class="black">E-faktur:</b> <span class="grey">Belum tersedia</span></p> <p class="pull-right"><a href=""><i class="material-icons md-18">rotate_right</i> Pesan ulang</a></p>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="tracking-status">
                                            <ul class="tracker-progress list-unstyled">
                                                <li class="active"><i class="material-icons md-24">schedule</i></li>
                                                <li class=""><i class="material-icons md-24">done</i></li>
                                                <li class=""><i class="material-icons md-24">local_shipping</i></li>
                                                <li class=""><i class="material-icons md-24">done_all</i></li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <p><b>Status:</b> <span class="red"><b>2</b></span> dari <span class="red"><b>3</b></span> pesanan diterima dan sedang dipersiapkan penjual</p>
                                            <p class="grey"><small>18 Agustus 2017 pukul 21:37</small></p>
                                        </div>
                                        <div class="collapse more-detail" id="collapseExample">
                                            <table>
                                                <tr>
                                                    <td width="50%">
                                                        <div class="detail pull-left">
                                                            <p>
                                                                <b>BCA KCP Mal Pondok Indah II</b><br>
                                                                Jl. Kencana Indah III No. 28
                                                                Pondok Indah, Jakarta Selatan
                                                                19200, Indonesia
                                                            </p>
                                                        </div>
                                                        <div class="detail pull-left">
                                                            <p><i class="material-icons md-18">person</i> Baydiwo <br>
                                                            <i class="material-icons md-18">call</i> Baydiwo</p>
                                                        </div>
                                                    </td>
                                                    <td width="50%">
                                                        <p><b>Metode pengiriman</b> <br>
                                                            Bizzy Fulfillment
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table class="table table-bordered table-myorder">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <a href="#">
                                                                    <img class="media-object" src="build/img/laptop-thumb.png" alt="...">
                                                                    </a>
                                                                </div>
                                                                <div class="media-body">
                                                                    <p><b>Apple Macbook Pro 256SSD - SV100021HC (Extended Warranty)</b></p>
                                                                    <span>Estimasi pengiriman <i>3-7 hari</i></span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>Status: <br> <span class="green">Diterima</span></td>
                                                        <td>Keterangan: <br> - </td>
                                                        <td><a href="#" class="btn btn-primary btn-alternative-vendor" disabled>Penjual alternatif</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="media">
                                                                <div class="media-left">
                                                                    <a href="#">
                                                                    <img class="media-object" src="build/img/laptop-thumb.png" alt="...">
                                                                    </a>
                                                                </div>
                                                                <div class="media-body">
                                                                    <p><b>Apple Macbook Pro 256SSD - SV100021HC (Extended Warranty)</b></p>
                                                                    <span>Estimasi pengiriman <i>3-7 hari</i></span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>Status: <br> <span class="red">Diterima</span></td>
                                                        <td>Keterangan: <br> - </td>
                                                        <td><a href="#" class="btn btn-primary btn-alternative-vendor">Penjual alternatif</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="table table-no-bordered table-order-total">
                                                <tbody>
                                                    <tr>
                                                        <td>Subtotal</td>
                                                        <td>Rp99.9999</td>
                                                    </tr>
                                                    <tr>
                                                        <td>PPn 10%</td>
                                                        <td>Rp99.9999</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pengiriman</td>
                                                        <td>Rp99.9999</td>
                                                    </tr>
                                                    <tr class="discount">
                                                        <td>Discount (Bizzy Berkontribusi)</td>
                                                        <td>-Rp99.9999</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total tagihan</td>
                                                        <td>Rp999.999</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div> <!-- collapse -->
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a class="btn-more-detail" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" data-text-swap="Sembunyikan detil pesanan" data-text-original="Lihat detil pesanan">Lihat detil pesanan</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> <!-- table responsive -->
                </div> <!-- row -->
            </div> <!-- bg white -->

            <div class="row bg-white-no-padding no-padding">
                <div class="col-xs-12 no-padding">
                    <div class="table-responsive">
                        <table class="table table-bordered table-order-total">
                            <thead>
                                <tr>
                                    <td colspan="2">Tagihan keseluruhan pesanan</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Subtotal</td>
                                    <td>Rp99.9999</td>
                                </tr>
                                <tr>
                                    <td>PPn 10%</td>
                                    <td>Rp99.9999</td>
                                </tr>
                                <tr>
                                    <td>Pengiriman</td>
                                    <td>Rp99.9999</td>
                                </tr>
                                <tr class="discount">
                                    <td>Discount (Bizzy Berkontribusi)</td>
                                    <td>-Rp99.9999</td>
                                </tr>
                                <tr>
                                    <td>Total keseluruhan</td>
                                    <td>Rp999.999</td>
                                </tr>
                            </tbody>
                        </table>
                    </div> <!-- table responsive-->
                </div>
            </div> <!-- bgwhite -->
        </div>
    </div> <!-- row bg-white -->
</div>
<?php include 'footer.php'; ?>
