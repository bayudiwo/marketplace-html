// Mega Menu
jQuery(document).ready(function($){
    //open/close mega-navigation
    $('.cd-dropdown-trigger').on('click', function(event){
        event.preventDefault();
        toggleNav();
    });

    //close meganavigation
    $('.cd-dropdown .cd-close , .overlay , .search , .col-last').on('click', function(event){
        event.preventDefault();
        toggleNav();
    });

    //on mobile - open submenu
    $('.has-children').children('a').on('click', function(event){
        //prevent default clicking on direct children of .has-children
        event.preventDefault();
        var selected = $(this);
        selected.next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('move-out');
    });

    //on desktop - differentiate between a user trying to hover over a dropdown item vs trying to navigate into a submenu's contents
    var submenuDirection = ( !$('.cd-dropdown-wrapper').hasClass('open-to-left') ) ? 'right' : 'left';
    $('.cd-dropdown-content').menuAim({
        activate: function(row) {
            $(row).children().addClass('is-active').removeClass('fade-out');
            if( $('.cd-dropdown-content .fade-in').length == 0 ) $(row).children('ul').addClass('fade-in');
        },
        deactivate: function(row) {
            $(row).children().removeClass('is-active');
            if( $('li.has-children:hover').length == 0 || $('li.has-children:hover').is($(row)) ) {
                $('.cd-dropdown-content').find('.fade-in').removeClass('fade-in');
                $(row).children('ul').addClass('fade-out')
            }
        },
        exitMenu: function() {
            $('.cd-dropdown-content').find('.is-active').removeClass('is-active');
            return true;
        },
        submenuDirection: submenuDirection,
    });

    //submenu items - go back link
    $('.go-back').on('click', function(){
        var selected = $(this),
            visibleNav = $(this).parent('ul').parent('.has-children').parent('ul');
        selected.parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('move-out');
    });

    function toggleNav(){
        var navIsVisible = ( !$('.cd-dropdown').hasClass('dropdown-is-active') ) ? true : false;
        var overlayVisible = ( !$('.overlay').hasClass('active') ) ? true : false;
        $('.cd-dropdown').toggleClass('dropdown-is-active', navIsVisible);
        $('.cd-dropdown-trigger').toggleClass('dropdown-is-active', navIsVisible);
        $('.overlay').toggleClass('active' , overlayVisible);
        if( !navIsVisible ) {
            $('.cd-dropdown').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(){
                $('.has-children ul').addClass('is-hidden');
                $('.move-out').removeClass('move-out');
                $('.is-active').removeClass('is-active');
            });
        }
    }

    //IE9 placeholder fallback
    //credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
    if(!Modernizr.input.placeholder){
        $('[placeholder]').focus(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
            }
        }).blur(function() {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.val(input.attr('placeholder'));
            }
        }).blur();
        $('[placeholder]').parents('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            })
        });
    }
});

//hamburger menu
var menuButton = document.getElementById('menuButton');
menuButton.addEventListener('click', function (e) {
    menuButton.classList.toggle('is-active');
    e.preventDefault();
});

//sticky header
// $(window).scroll(function() {
// if ($(this).scrollTop() > 1){
//     $('header').addClass("sticky");
//   }
//   else{
//     $('header').removeClass("sticky");
//   }
// });
// $("header").sticky({topSpacing:0,className:'sticky'});

// Box kotak belanja
// $('#order-box').on('click', function(e) {
//     e.preventDefault();
//     $('.order-wrapper').fadeToggle(120);
// });
$('#order-box').click(function(event) {
  $('.overlay , .cart-wrapper').toggleClass('active');
  $(this).toggleClass('actived');
});


// see all vendor
// $('#see-all-vendor').toggle(function (e) {
//     e.preventDefault();
//     console.log('test');
//     $(".list-vendor").addClass("show");
// }, function () {
//     $(".list-vendor").removeClass("show");
// });
$('#see-all-vendor').on('click', function(e) {
    console.log('s');
    $("#list-vendor").css({overflowY:'scroll'});
})


$(document).ready(function() {
    // $("#prefered").hide(); //hide your div initially
    // var topOfOthDiv = $(".box-details").offset().top;
    // $(window).scroll(function() {
    //     if($(window).scrollTop() > topOfOthDiv) { //scrolled past the other div?
    //         // $("#prefered").show();
    //         $("#prefered").css({left:'-80px'}); //reached the desired point -- show div
    //     }
    // });

    //vendor selection
    $('.product-details dl, .v').hide();
    $('#list-vendor li.active').each(function(){
       a = $(this).attr('id');
       $('.'+a).fadeIn(300);
    });
    $('#list-vendor li[id]').click(function() {
        var theid = this.id;

        if($('.'+theid).is(':visible') == false ) {
            $('.product-details dl, .v').hide();
            $('#list-vendor li').each(function(){
               $(this).removeClass('active');
            });
            $('.'+theid).fadeIn(300);
            $('#list-vendor #'+theid).addClass('active');
        }
    });

    //login toggle
    $('#login-toggle').on('click', function(e) {
         if ( $('#login-wrapper').css('visibility') == 'hidden' ) {
            $('#login-wrapper').css('visibility','visible');
            $('#btn-login-wrapper').addClass('login-active');
         }
        else {
            $('#login-wrapper').css('visibility','hidden');
            $('#btn-login-wrapper').removeClass('login-active');
        }
    })


    // $("#buy").on("click", function(e) {
    //     $.alert({
    //         title: 'Alert!',
    //         content: 'Simple alert!',
    //     });
    // });

    //js for order tracking
    $('#buy').on('click', function () {
        $.confirm({
            title: false,
            content: 'url:vendor.html',
            animation: 'zoom',
            columnClass: 'medium',
        });
    });

    $('#vendor1').on('click', function () {
        console.log('test');
        $.confirm({
            title: false,
            content: 'Apakah Anda yakin ingin memilih Savemax sebagai penjual alternatif?',
            animation: 'zoom',
            columnClass: 'medium',
            buttons: {
                'confirm': {
                    text: 'Proceed',
                    btnClass: 'btn-info',
                    action: function () {
                        $.confirm({
                            title: 'This maybe critical',
                            content: 'Critical actions can have multiple confirmations like this one.',
                            icon: 'fa fa-warning',
                            animation: 'zoom',
                            closeAnimation: 'zoom',
                            buttons: {
                                confirm: {
                                    text: 'Yes, sure!',
                                    btnClass: 'btn-warning',
                                    action: function () {
                                        $.alert('A very critical action <strong>triggered!</strong>');
                                    }
                                },
                                cancel: function () {
                                    $.alert('you clicked on <strong>cancel</strong>');
                                }
                            }
                        });
                    }
                },
                cancel: function () {
                    $.alert('you clicked on <strong>cancel</strong>');
                },
                moreButtons: {
                    text: 'something else',
                    action: function () {
                        $.alert('you clicked on <strong>something else</strong>');
                    }
                },
            }
        });
    });

    $(".btn-more-detail").on("click", function() {
      var el = $(this);
      el.text() == el.data("text-swap")
        ? el.text(el.data("text-original"))
        : el.text(el.data("text-swap"));
    });
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
// eof order tracking

