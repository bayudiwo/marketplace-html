<footer>
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-unstyled list-inline list-services">
                            <li>
                                <p><b>Aman</b></p>
                                <img src="build/img/thumb.png" alt="">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </li>
                            <li>
                                <p><b>Aman</b></p>
                                <img src="build/img/thumb.png" alt="">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </li>
                            <li>
                                <p><b>Aman</b></p>
                                <img src="build/img/thumb.png" alt="">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </li>
                            <li>
                                <p><b>Aman</b></p>
                                <img src="build/img/thumb.png" alt="">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </li>
                            <li>
                                <p><b>Aman</b></p>
                                <img src="build/img/thumb.png" alt="">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- footer wrapper -->
        <div class="footer-wrapper newsletter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-7">
                        <p><i class="material-icons md-18">email</i> Daftar Newsletter Bizzy &amp; Dapatkan Voucher <b>Senilai 100rb!</b></p>
                    </div>
                    <div class="col-lg-5 col-sm-5 pull-right">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Email Anda">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-orange" type="button">Daftar</button>
                            </span>
                        </div><!-- /input-group -->
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- footer-wrapper -->
        <div class="footer-wrapper cards">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <img src="build/img/cards.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </div> <!-- footer wrapper cards -->
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="list-unstyled list-inline list-sitemap">
                        <li>
                            <ul class="list-unstyled">
                                <li>Layanan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                            </ul>
                        </li>
                        <li>
                            <ul class="list-unstyled">
                                <li>Layanan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                            </ul>
                        </li>
                        <li>
                            <ul class="list-unstyled">
                                <li>Layanan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                                <li>Bantuan Pelanggan</li>
                            </ul>
                        </li>
                        <li>
                            <h5>Hubungi Kami</h5>
                            <p>021-2978-4578 <br>
                                support@bizzy.co.id <br><br>
                                Senin-Jumat, 09.00 - 18.00</p>

                        </li>
                    </ul>
                </div>
            </div>
        </div> <!-- footer-wrapper -->
        <div class="footer-wrapper newsletter">
            <div class="container">
                <div class="row">
                    <div class="cards logo">
                        <img src="build/img/logo-footer.png" alt=""> Copyrights 2016
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="build/js/vendor/jquery.menu-aim.js" type="text/javascript"></script>
    <script src="build/js/vendor/bootstrap.min.js"></script>
    <script src="build/js/app.js"></script>
</body>
</html>
