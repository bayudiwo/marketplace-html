<?php include 'header.php'; ?>
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Library</a></li>
                        <li class="active">Data</li>
                    </ol>
                </div>
            </div>
        </div>
    </section> <!-- breadcrumbs -->
    <article class="product-detail">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5">
                    <div class="left">
                        <h1>Apple Macbook Pro 256SSD - SV100021HC (Extended Warranty)</h1>
                        <div class="sub-title"><span>SKU: MAC0190290</span> <span>Produk: <a href="#">Apple</a></span> <span>Harga Mulai Dari: Rp.1.000.000</span></div>
                        <!-- space for images -->
                        <img src="build/img/prd.png" alt="" class="img-responsive">
                        <div class="protection">
                            <i class="material-icons md-18">verified_user</i> <div class="text">
                                <b>100% Perlindungan Pembeli!</b> <br>
                                Full Refund if you don't receive your order <br> Partial Refund, if the item is not as described
                            </div>
                        </div>
                        <div class="btn-group" role="group" >
                            <button type="button" class="btn btn-default col-xs-6"><i class="material-icons md-18">repeat</i> Bandingkan</button>
                            <button type="button" class="btn btn-default col-xs-6 gold"><i class="material-icons md-18">star</i> Favorit</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="box-details">
                        <div class="box type">Pilih ukuran</div>
                        <div class="box value">
                            <ul class="list-unstyled list-inline list-select list-size">
                                <li>38</li>
                                <li class="active">38</li>
                                <li>38</li>
                                <li>38</li>
                                <li>38</li>
                            </ul>
                        </div>
                        <div class="swatch-attribute color" attribute-code="color" attribute-id="93">
                            <span class="swatch-attribute-label">Color</span>
                            <span class="swatch-attribute-selected-option"></span>
                            <div class="swatch-attribute-options clearfix">
                                <div class="swatch-option color" option-type="1" option-id="49" option-label="Black" option-tooltip-thumb="" option-tooltip-value="#000000" "="" style="background: #000000 no-repeat center; background-size: initial;"></div>
                                <div class="swatch-option color" option-type="1" option-id="50" option-label="Blue" option-tooltip-thumb="" option-tooltip-value="#1857f7" "="" style="background: #1857f7 no-repeat center; background-size: initial;"></div>
                                <div class="swatch-option color" option-type="1" option-id="57" option-label="Purple" option-tooltip-thumb="" option-tooltip-value="#ef3dff" "="" style="background: #ef3dff no-repeat center; background-size: initial;"></div>
                            </div>
                        </div>
                        <div class="box type saler">Pilih penjual</div>
                        <div class="box vendor">
                            <div class="prefered" id="prefered"><i class="material-icons md-18">stars</i> Prefered</div>
                            <ul class="list-unstyled list-inline list-vendor" id="list-vendor">
                                <li class="active" id="vendor-Subtech">
                                    <div class="name"><a href="">Subtech</a></div>
                                    <div class="price">Rp 15.000.000</div>
                                    <div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span> <i class="material-icons checked">check</i></div>
                                </li>
                                <li id="vendor-Viraindo">
                                    <div class="name"><a href="" data-toggle="tooltip" data-placement="right" title="PT Mulia Mega Pratama" >1. PT Mulia ...</a>
                                    </div>
                                    <div class="price">Rp 15.000.000</div>
                                    <div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span> <i class="material-icons checked">check</i></div>
                                </li>
                                <li id="vendor-Tzone">
                                    <div class="name"><a href="" data-toggle="tooltip" data-placement="right" title="PT Mulia Mega Pratama">2. Tzone megapr...</a></div>
                                    <div class="price">Rp 15.000.000</div>
                                    <div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span> <i class="material-icons checked">check</i></div>
                                </li>
                                <li>
                                    <div class="name"><a href="">3. Subtech</a></div>
                                    <div class="price">Rp 15.000.000</div>
                                    <div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span> <i class="material-icons checked">check</i></div>
                                </li>
                                <li>
                                    <div class="name"><a href="">4. Subtech</a></div>
                                    <div class="price">Rp 15.000.000</div>
                                    <div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span> <i class="material-icons checked">check</i></div>
                                </li>
                                <li>
                                    <div class="name"><a href="">5. Subtech</a></div>
                                    <div class="price">Rp 15.000.000</div>
                                    <div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span> <i class="material-icons checked">check</i></div>
                                </li>
                                <li>
                                    <div class="name"><a href="">6. Subtech</a></div>
                                    <div class="price">Rp 15.000.000</div>
                                    <div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span> <i class="material-icons checked">check</i></div>
                                </li>
                                <li>
                                    <div class="name"><a href="">7. Subtech</a></div>
                                    <div class="price">Rp 15.000.000</div>
                                    <div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span> <i class="material-icons checked">check</i></div>
                                </li>
                            </ul>
                            <a class="see-all" id="see-all-vendor">Lihat semua penjual</a>
                        </div>
                        <div class="box product-details">
                            <dl class="dl-horizontal dl-default vendor-Subtech">
                                <dt><i class="material-icons md-18">archive</i> Stok tersedia</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">unarchive</i> UoM</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">play_for_work</i> Berat</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">flip_to_front</i> Dimensi paket</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">verified_user</i> Garansi</dt>
                                <dd>1 year official warranty</dd>
                                <dt><i class="material-icons md-18">local_shipping</i> Estimasi pengiriman</dt>
                                <dd>3-5 hari <span>(Jakarta)</span></dd>
                                <dt><i class="material-icons md-18">undo</i> Kebijakan pengembalian</dt>
                                <dd>1-1000</dd>
                            </dl>
                            <dl class="dl-horizontal dl-default vendor-Viraindo">
                                <dt><i class="material-icons md-18">archive</i> Stok tersedia</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">unarchive</i> UoM</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">play_for_work</i> Berat</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">flip_to_front</i> Dimensi paket</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">verified_user</i> Garansi</dt>
                                <dd>1 year official warranty</dd>
                                <dt><i class="material-icons md-18">local_shipping</i> Estimasi pengiriman</dt>
                                <dd>3-5 hari <span>(Jakarta)</span></dd>
                                <dt><i class="material-icons md-18">undo</i> Kebijakan pengembalian</dt>
                                <dd>1-1000</dd>
                            </dl>
                            <dl class="dl-horizontal dl-default vendor-Tzone">
                                <dt><i class="material-icons md-18">archive</i> Stok tersedia</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">unarchive</i> UoM</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">play_for_work</i> Berat</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">flip_to_front</i> Dimensi paket</dt>
                                <dd>1-1000</dd>
                                <dt><i class="material-icons md-18">verified_user</i> Garansi</dt>
                                <dd>1 year official warranty</dd>
                                <dt><i class="material-icons md-18">local_shipping</i> Estimasi pengiriman</dt>
                                <dd>3-5 hari <span>(Jakarta)</span></dd>
                                <dt><i class="material-icons md-18">undo</i> Kebijakan pengembalian</dt>
                                <dd>1-1000</dd>
                            </dl>
                        </div>
                    </div> <!-- box-details -->
                </div> <!-- col-lg-4 -->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="box buy">
                        <div class="row">
                            <div class="col-xs-12">Beli Grosir</div>
                            <div class="col-xs-4 first">
                                <span>1-5 unit</span>
                                Rp 15.000.000
                            </div>
                            <div class="col-xs-4">
                                <span>1-5 unit</span>
                                Rp 15.000.000
                            </div>
                            <div class="col-xs-4">
                                <span>1-5 unit</span>
                                Rp 15.000.000
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="counter pull-left">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-default">-</button>
                                        <input type="text" name="quantity" value="1" maxlength="2" max="10" size="1" id="number" class="input-number" />
                                        <button type="button" class="btn btn-default">+</button>
                                    </div>
                                </div>
                                <div class="total pull-left">
                                    <p>Total Harga</p>
                                    <p class="price">Rp77.824.000</p>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button class="btn btn-blue btn-fluid">Beli</button>
                            </div>
                        </div>
                    </div>
                    <div class="box-details vendor-information">
                        <div class="box type">Informasi penjual</div>
                        <div class="v vendor-Viraindo">
                            <div class="box the-vendor">
                                <a href=""><b>Viraindo</b></a>
                                <p>Manufacturer, Trading Company <br> Jakarta, Indonesia</p>
                            </div>
                            <div class="box">
                                <dl class="dl-horizontal reports">
                                    <dt>Stok tersedia</dt>
                                    <dd>5.0</dd>
                                    <dt>Overall rating</dt>
                                    <dd><div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span></div></dd>
                                </dl>
                            </div>
                            <div class="box">
                                <dl class="dl-horizontal reports">
                                    <dt>Stok tersedia</dt>
                                    <dd>5.0</dd>
                                    <dt>UoM</dt>
                                    <dd>5.0</dd>
                                    <dt>Berat</dt>
                                    <dd>5.0</dd>
                                    <dt>Response rate</dt>
                                    <dd>5.0</dd>
                                    <dt>Garansi</dt>
                                    <dd>5.0</dd>
                                    <dt>Estimasi pengiriman</dt>
                                    <dd>5.0</dd>
                                    <dt>Kebijakan pengembalian</dt>
                                    <dd>5.0</dd>
                                </dl>
                            </div>
                        </div>
                        <div class="v vendor-Subtech">
                            <div class="box the-vendor">
                                <a href=""><b>Subtech</b></a>
                                <p>Manufacturer, Trading Company <br> Jakarta, Indonesia</p>
                            </div>
                            <div class="box">
                                <dl class="dl-horizontal reports">
                                    <dt>Stok tersedia</dt>
                                    <dd>5.0</dd>
                                    <dt>Overall rating</dt>
                                    <dd><div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span></div></dd>
                                </dl>
                            </div>
                            <div class="box">
                                <dl class="dl-horizontal reports">
                                    <dt>Stok tersedia</dt>
                                    <dd>5.0</dd>
                                    <dt>UoM</dt>
                                    <dd>5.0</dd>
                                    <dt>Berat</dt>
                                    <dd>5.0</dd>
                                    <dt>Response rate</dt>
                                    <dd>5.0</dd>
                                    <dt>Garansi</dt>
                                    <dd>5.0</dd>
                                    <dt>Estimasi pengiriman</dt>
                                    <dd>5.0</dd>
                                    <dt>Kebijakan pengembalian</dt>
                                    <dd>5.0</dd>
                                </dl>
                            </div>
                        </div>
                        <div class="v vendor-Tzone">
                            <div class="box the-vendor">
                                <a href=""><b>Tzone</b></a>
                                <p>Manufacturer, Trading Company <br> Jakarta, Indonesia</p>
                            </div>
                            <div class="box">
                                <dl class="dl-horizontal reports">
                                    <dt>Stok tersedia</dt>
                                    <dd>5.0</dd>
                                    <dt>Overall rating</dt>
                                    <dd><div class="rating"><i class="material-icons star">star_rate</i> 5.0/<span>5.0</span></div></dd>
                                </dl>
                            </div>
                            <div class="box">
                                <dl class="dl-horizontal reports">
                                    <dt>Stok tersedia</dt>
                                    <dd>5.0</dd>
                                    <dt>UoM</dt>
                                    <dd>5.0</dd>
                                    <dt>Berat</dt>
                                    <dd>5.0</dd>
                                    <dt>Response rate</dt>
                                    <dd>5.0</dd>
                                    <dt>Garansi</dt>
                                    <dd>5.0</dd>
                                    <dt>Estimasi pengiriman</dt>
                                    <dd>5.0</dd>
                                    <dt>Kebijakan pengembalian</dt>
                                    <dd>5.0</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <div class="help">
                        <p><b>Butuh bantuan? <a href="tel:+622129784578">622129784578</a></b></p>
                    </div>
                    <a href="" class="terms">Syarat &amp; Ketentuan</a>
                </div> <!-- col-lg-2 -->
            </div> <!-- row -->
        </div> <!-- container -->
        <div class="divider"></div>
        <div class="description">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Deskripsi</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur aspernatur odio provident eum fugit eaque atque quasi magni cupiditate incidunt nihil, sapiente dicta soluta rem similique. Perferendis ullam minima beatae?</p>
                        <p><b>Performa Mengesankan</b></p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error illum sequi praesentium minus ullam dignissimos delectus, excepturi culpa hic nemo inventore, a ad fugit quidem aperiam quo nesciunt consequatur. Consectetur!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row ">
                <div class="col-md-6 spec">
                    <h3>Spesifikasi</h3>
                    <dl class="dl-horizontal dl-default">
                        <dt>Platform</dt>
                        <dd>Notebook</dd>
                        <dt>Platform</dt>
                        <dd>Notebook</dd>
                        <dt>Platform</dt>
                        <dd>Intel® Celeron® Processor N3050 (1.6 GHz, 2M Cache) up to 2.16 GHz </dd>
                        <dt>Platform</dt>
                        <dd>Notebook</dd>
                        <dt>Platform</dt>
                        <dd>Notebook</dd>
                    </dl>
                </div>
                <div class="col-md-6">
                    <h3>Di dalam Kemasan</h3>
                    <p>Notebook <br> Intel® Celeron® Processor N3050 (1.6 GHz, 2M Cache) up to 2.16 GHz </p>
                </div>
            </div>
        </div>
    </article>
<?php include 'footer.php'; ?>
