<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title></title>

    <!-- Bootstrap -->
    <link href="build/css/bootstrap.min.css" rel="stylesheet">
    <link href="build/css/jquery-confirm.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="build/css/global.css" rel="stylesheet">
    <link href="build/css/media.css" rel="stylesheet">

    <script src="build/js/vendor/modernizr.js" type="text/javascript"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="overlay"></div>
<nav class="cart-wrapper" id="cart-wrapper">
    <div class="order-wrapper">
        <h5>Kotak Belanja masih kosong</h5>
        <button class="btn btn-checkout btn-green">Mulai Belanja</button>
        <h5>Pesanan Anda <span>(5)</span></h5>
        <div class="order-list">
            <ul class="list-unstyled">
                <li>
                    <div class="thumb dilb">
                        <img src="build/img/thumb.png" alt="">
                    </div>
                    <div class="product dilb">
                        <a class="name" href="#">Panasonic Laptop</a><br>
                        <div class="total">Total Rp 360.000</div>
                    </div>
                    <div class="counter dilb">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default">-</button>
                            <input type="text" name="quantity" value="1" maxlength="2" max="10" size="1" id="number" class="input-number" />
                            <button type="button" class="btn btn-default">+</button>
                        </div>

                        <div class="delete dilb">
                            <a href="#"><i class="material-icons md-18">clear</i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="thumb dilb">
                        <img src="build/img/thumb.png" alt="">
                    </div>
                    <div class="product dilb">
                        <a class="name" href="#">Sinar Dunia Buku Tulis</a><br>
                        <div class="total">Total Rp 360.000</div>
                    </div>
                    <div class="counter dilb">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default">-</button>
                            <input type="text" name="quantity" value="1" maxlength="2" max="10" size="1" id="number" class="input-number" />
                            <button type="button" class="btn btn-default">+</button>
                        </div>

                        <div class="delete dilb">
                            <a href="#"><i class="material-icons md-18">clear</i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="thumb dilb">
                        <img src="build/img/thumb.png" alt="">
                    </div>
                    <div class="product dilb">
                        <a class="name" href="#">Panasonic Laptop</a><br>
                        <div class="total">Total Rp 360.000</div>
                    </div>
                    <div class="counter dilb">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default">-</button>
                            <input type="text" name="quantity" value="1" maxlength="2" max="10" size="1" id="number" class="input-number" />
                            <button type="button" class="btn btn-default">+</button>
                        </div>

                        <div class="delete dilb">
                            <a href="#"><i class="material-icons md-18">clear</i></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div> <!-- order list -->
        <div class="divider"></div>
        <div class="previous-order">
            <h5>Pesanan Sebelumnya</h5>
            <table class="table table-condensed">
                <tr>
                    <td>21 Agustus 2016</td>
                    <td>12:30 PM</td>
                    <td>2 Jumlah</td>
                    <td><a href="#">Reorder <i class="material-icons md-18">rotate_right</i></a></td>
                </tr>
                <tr>
                    <td>21 Agustus 2016</td>
                    <td>12:30 PM</td>
                    <td>2 Jumlah</td>
                    <td><a href="#">Reorder <i class="material-icons md-18">rotate_right</i></a></td>
                </tr>
                <tr>
                    <td>21 Agustus 2016</td>
                    <td>12:30 PM</td>
                    <td>2 Jumlah</td>
                    <td><a href="#">Reorder <i class="material-icons md-18">rotate_right</i></a></td>
                </tr>
            </table>
        </div>  <!-- prev order -->
        <div class="divider"></div>
        <div class="total">
            <h5 class="pull-left">Subtotal</h5>
            <h5 class="pull-right">Rp 78.000.000</h5>
        </div>
        <div class="clearfix"></div>
        <button class="btn btn-checkout btn-green">Checkout</button>
        <button class="btn btn-white btn-checkout btn-default">Generate Quote</button>
    </div><!-- order container -->
</nav>

    <header>
        <div class="top-header mobile-hidden">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="contacts">
                            <ul class="list-inline list-unstyled listing">
                                <li><a href="tel:+622129784578"><i class="material-icons md-18">local_phone</i> (021) 2978 4578</a></li>
                                <li><a href="mailto:support@bizzy.co.id"><i class="material-icons md-18">email</i> support@bizzy.co.id</a></li>
                            </ul>

                            <ul class="list-inline list-unstyled listing account">
                                <li><a href="#"><i class="material-icons md-18">person</i> Akun saya</a></li>
                                <li><a href="#"><i class="material-icons md-18">event_note</i> Histori pemesanan</a></li>
                                <li><a href="#"><i class="material-icons md-18">local_shipping</i> Tracking pemesanan</a></li>
                                <li><a href="#"><i class="material-icons m-18 star">star_rate</i> Produk favorit</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- top header -->
        <div class="bottom-header">
            <div class="container">
                <div class="row">
                    <div class="col-first">
                        <div class="logo">
                            <a href="#"><img src="build/img/logo.png" alt="Bizzy | Everything For Business"></a>
                        </div>
                        <!-- mega menu -->
                        <div class="main-menu">
                            <div class="cd-dropdown-wrapper">
                                <a class="cd-dropdown-trigger mobile-hidden" href="#" class="menu-button" id="menuButton"><!-- <span class="burger-icon"></span> --><i class="material-icons md-24">menu</i><b>KATEGORI</b></a>
                                <nav class="cd-dropdown">
                                    <h2>Bizzy.co.id</h2>
                                    <a href="#0" class="cd-close">Close</a>
                                    <ul class="cd-dropdown-content">
                                        <li class="has-children">
                                            <a href="http://codyhouse.co/?p=748">Clothing</a>

                                            <ul class="cd-secondary-dropdown is-hidden">
                                                <li class="go-back"><a href="#0">Menu</a></li>
                                                <li class="has-children">
                                                    <a href="http://codyhouse.co/?p=748">Komputer &amp; Laptop</a>

                                                    <ul class="is-hidden">
                                                        <li class="go-back"><a href="#0">Clothing</a></li>
                                                        <li class="see-all"><a href="http://codyhouse.co/?p=748">All Accessories</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Glasses</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Gloves</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Jewellery</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Scarves</a></li>
                                                    </ul>
                                                </li>

                                                <li class="has-children">
                                                    <a href="http://codyhouse.co/?p=748">Bottoms</a>

                                                    <ul class="is-hidden">
                                                        <li class="go-back"><a href="#0">Clothing</a></li>
                                                        <li class="see-all"><a href="http://codyhouse.co/?p=748">All Bottoms</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Casual Trousers</a></li>
                                                        <li><a href="#0">Leggings</a></li>
                                                        <li><a href="#0">Shorts</a></li>
                                                    </ul>
                                                </li>

                                                <li class="has-children">
                                                    <a href="http://codyhouse.co/?p=748">Jackets</a>

                                                    <ul class="is-hidden">
                                                        <li class="go-back"><a href="#0">Clothing</a></li>
                                                        <li class="see-all"><a href="http://codyhouse.co/?p=748">All Jackets</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Blazers</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Bomber jackets</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Denim Jackets</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Duffle Coats</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Leather Jackets</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Parkas</a></li>
                                                    </ul>
                                                </li>

                                                <li class="has-children">
                                                    <a href="http://codyhouse.co/?p=748">Tops</a>

                                                    <ul class="is-hidden">
                                                        <li class="go-back"><a href="#0">Clothing</a></li>
                                                        <li class="see-all"><a href="http://codyhouse.co/?p=748">All Tops</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Cardigans</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Coats</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Polo Shirts</a></li>
                                                        <li><a href="http://codyhouse.co/?p=748">Shirts</a></li>
                                                        <li class="has-children">
                                                            <a href="#0">T-Shirts</a>

                                                            <ul class="is-hidden">
                                                                <li class="go-back"><a href="#0">Tops</a></li>
                                                                <li class="see-all"><a href="http://codyhouse.co/?p=748">All T-shirts</a></li>
                                                                <li><a href="http://codyhouse.co/?p=748">Plain</a></li>
                                                                <li><a href="http://codyhouse.co/?p=748">Print</a></li>
                                                                <li><a href="http://codyhouse.co/?p=748">Striped</a></li>
                                                                <li><a href="http://codyhouse.co/?p=748">Long sleeved</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="http://codyhouse.co/?p=748">Vests</a></li>
                                                    </ul>
                                                </li>
                                            </ul> <!-- .cd-secondary-dropdown -->
                                        </li> <!-- .has-children -->
                                    </ul> <!-- .cd-dropdown-content -->
                                </nav> <!-- .cd-dropdown -->
                            </div> <!-- .cd-dropdown-wrapper -->
                        </div>
                    </div> <!-- col-first -->
                    <div class="col-middle">
                        <!-- mobile menu -->
                        <a class="cd-dropdown-trigger mobile-only dropdown-toggle toggle-menu btn btn-navbar" href="" class="menu-button" id="menuButton"><!-- <span class="burger-icon"></span> --><i class="material-icons md-24">menu</i></a>
                        <!-- search -->
                        <div class="search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Tulis produk yang anda cari...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="material-icons md-18">search</i> </button>
                                </span>
                            </div><!-- /input-group -->
                        </div> <!-- eof search -->
                    </div>
                    <div class="col-last">
                        <div class="btn-group" role="group" id="btn-login-wrapper" >
                            <button type="button" class="btn btn-default" id="login-toggle"><i class="material-icons md-18">exit_to_app</i> <span class="mobile-hidden">MASUK</span></button>
                            <button type="button" class="btn btn-default"><i class="material-icons md-18">person_add</i> <span class="mobile-hidden">DAFTAR</span></button>
                        </div>
                        <div class="login" id="login-wrapper">
                            <form>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="username" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Ingat saya
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-default btn-green btn-fluid">Login</button>
                                <a class="forgot" href="#">Lupa password</a>
                            </form>
                        </div>
                        <div class="identity" style="display: none;">
                            <a href="#"><i class="material-icons md-36">perm_identity</i> <div class="details"><span class="username">Bayu Adi Wibowo</span> <br> <span class="company">PT. Bizzy Indonesia</span></div></a>
                        </div>
                        <button type="button" class="btn btn-default btn-orange" id="order-box"><i class="material-icons md-18">shopping_cart</i> <span class="mobile-hidden">KOTAK BELANJA (12)</span></button>

                        <div class="tooltip bottom" role="tooltip">
                            <div class="tooltip-arrow"></div>
                            <div class="tooltip-inner">
                                Produk berhasil ditambah ke kotak belanja!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- bottom header -->
    </header>
