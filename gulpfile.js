var gulp        = require('gulp');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglify');
var imagemin    = require('gulp-imagemin');
var sourcemaps  = require('gulp-sourcemaps');
var del         = require('del');
var prefix      = require('gulp-autoprefixer');
var plumber     = require('gulp-plumber');
var watch       = require('gulp-watch');
var sass        = require('gulp-sass');
var browserSync = require('browser-sync').create();

var paths = {
    scripts: 'src/js/*.js',
    scriptsVendor: 'src/js/vendor/*.js',
    images: 'src/img/**/*',
    // scss: ['src/scss/*.scss','src/css/media/**/*.scss','!src/scss/components/**/*.scss'],
    scss: 'src/scss/**/*.scss',
    cssVendor: 'src/css/**/*.css',
};

// Not all tasks need to use streams
// A gulpfile is just another node program and you can use any package available on npm
gulp.task('clean', function() {
    // You can use multiple globbing patterns as you would with `gulp.src`
    return del(['build']);
});

// sync browser
gulp.task('browserSync', function() {
  browserSync.init({
    // server: {
    //   baseDir: 'app',
    // },
    proxy: 'http://dev.bizzymarketplace:8888/'
  })
});

gulp.task('sass', function () {
    return gulp.src(paths.scss)
    .pipe(plumber())
    .pipe(prefix({
        browsers: ['last 3 versions'],
        cascade: true
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    // .pipe(concat('style.min.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('css_copy', function () {
    return gulp.src(paths.cssVendor)
        .pipe(gulp.dest('build/css'));
});

gulp.task('scripts', function() {
    // Minify and copy all JavaScript (except vendor scripts)
    // with sourcemaps all the way down
    return gulp.src(paths.scripts)
    .pipe(sourcemaps.init())
    .pipe(uglify())
    // .pipe(concat('all.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('js_copy', function() {
    return gulp.src(paths.scriptsVendor)
        .pipe(gulp.dest('build/js/vendor'))
});

// Copy all static images
gulp.task('images', function() {
    return gulp.src(paths.images)
// Pass in options to the task
.pipe(imagemin({optimizationLevel: 5}))
.pipe(gulp.dest('build/img'));
});

gulp.task('font', function() {
    return gulp.src('src/fonts/**/*')
        .pipe(gulp.dest('build/fonts'))
});

// Rerun the task when a file changes
gulp.task('watch', ['browserSync', 'sass'], function() {
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.scriptsVendor, ['js_copy']);
    gulp.watch(paths.images, ['images']);
    gulp.watch(paths.scss, ['sass']);
    gulp.watch(paths.cssVendor, ['css_copy']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['scripts', 'js_copy', 'images' , 'sass' , 'css_copy', 'font']);
