<footer>
    <div class="footer-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-unstyled list-inline list-services">
                        <li>
                            <p><b>Aman</b></p>
                            <img src="build/img/thumb.png" alt="">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        </li>
                        <li>
                            <p><b>Aman</b></p>
                            <img src="build/img/thumb.png" alt="">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        </li>
                        <li>
                            <p><b>Aman</b></p>
                            <img src="build/img/thumb.png" alt="">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        </li>
                        <li>
                            <p><b>Aman</b></p>
                            <img src="build/img/thumb.png" alt="">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        </li>
                        <li>
                            <p><b>Aman</b></p>
                            <img src="build/img/thumb.png" alt="">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- footer wrapper -->
    <div class="footer-wrapper newsletter">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-7">
                    <p><i class="material-icons md-18">email</i> Daftar Newsletter Bizzy &amp; Dapatkan Voucher <b>Senilai 100rb!</b></p>
                </div>
                <div class="col-lg-5 col-sm-5 pull-right">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Email Anda">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-orange" type="button">Daftar</button>
                        </span>
                    </div><!-- /input-group -->
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- footer-wrapper -->
    <div class="footer-wrapper cards">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <img src="build/img/cards.png" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div> <!-- footer wrapper cards -->
    <div class="footer-wrapper">
        <div class="container">
            <div class="row">
                <ul class="list-unstyled list-inline list-sitemap">
                    <li>
                        <ul class="list-unstyled">
                            <li>Layanan Pelanggan</li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/layanan-pelanggan">Bantuan Pelanggan</a> <a></a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/cara-belanja">Panduan Belanja</a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/pembayaran">Pembayaran</a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/pengiriman">Pengiriman</a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/ketentuan-layanan">Ketentuan Layanan</a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/cara-pengembalian">Cara Pengembalian</a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/ada-pertanyaan">Ada Pertanyaan?</a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/hubungi-kami">Hubungi Kami</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <ul class="list-unstyled">
                            <li>Ada Pertanyaan?</li>
                            <li><a href="">Hubungi Kami</a></li>
                            <li><a href="">Tentang Bizzy</a></li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/tentang-kami">Tentang Kami</a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/syarat-dan-ketentuan">Syarat dan Ketentuan</a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/kebijakan-privasi">Kebijakan Privasi</a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/akun-saya">Akun Saya</a>
                            </li>
                            <li>
                                <a href="https://beta-mp.bizzy.co.id/admin/cms/wysiwyg/directive/___directive/e3tjb25maWcgcGF0aD0nd2ViL3NlY3VyZS9iYXNlX3VybCd9fQ,,/key/08f0fcd8f36d8b3716974476836e6fa52df5df9d1a12a39a0c422a95fa9b7b3d/order-status">Cek Status Pemesanan</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <ul class="list-unstyled">
                            <li>Berjualan di Bizzy</li>
                            <li><a href="">Hubungi Kami</a></li>
                            <li><a href="">Hubungi Kami</a></li>
                            <li><a href="">Hubungi Kami</a></li>
                        </ul>
                    </li>
                    <li class="list-free-delivery">
                        <ul class="list-unstyled">
                            <li class="orange">Gratis biaya pengiriman!</li>
                            <li>Gratis biaya pengiriman <br>untuk area Jakarta tanpa <br>minimum pembelian!</li>
                            <li><img src="" alt=""></li>
                        </ul>
                    </li>
                    <li>
                        <ul class="list-unstyled">
                            <li>Hubungi Kami</li>
                            <li>
                                <p>
                                    <i class="material-icons md-18">phone</i>021-2978-4578 <br>
                                    <span class="grey">Senin-Jumat, 09.00 - 18.00</span>
                                </p>
                            </li>
                            <li>
                                <a href="mailto:support@bizzy.co.id">support@bizzy.co.id</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div> <!-- footer-wrapper -->
    <div class="footer-wrapper newsletter">
        <div class="container">
            <div class="row">
                <div class="cards logo">
                    <img src="build/img/logo-footer.png" alt=""> Copyrights 2016
                </div>
            </div>
        </div>
    </div>
</footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="build/js/vendor/jquery.menu-aim.js" type="text/javascript"></script>
    <script src="build/js/vendor/bootstrap.min.js"></script>
    <script src="build/js/vendor/jquery.sticky.js"></script>
    <script src="build/js/vendor/jquery-confirm.js"></script>
    <script src="build/js/app.js"></script>
</body>
</html>
